import pickle
import os
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from google.auth.transport.requests import Request
from dotenv import dotenv_values


class GoogleConnection:
    def __init__(self, api, api_version, scope):

        cred = None

        pickle_file = f'token_{api}_{api_version}.pickle'
        if os.path.exists(pickle_file):
            with open(pickle_file, 'rb') as token:
                cred = pickle.load(token)

        if not cred or not cred.valid:
            if cred and cred.expired and cred.refresh_token:
                cred.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(dotenv_values('.env')["STORAGE_CLIENT_SECRET_FILE"], scope)
                cred = flow.run_local_server()

            with open(pickle_file, 'wb') as token:
                pickle.dump(cred, token)

        try:
            return build(api, api_version, credentials=cred)

        except Exception as e:
            print('Unable to connect.')
            print(e)
