from type_detector import TypeDetector
from folder_listener import FolderListener
from extract_file_text import ExtractFileText

for file in FolderListener().list_all_files(): # transformar essa lista em statica
    type = TypeDetector(file)
    type.discover()
    print("\n-----------------------\n")
    # break

print("================================================")
print("==================== ACABOU ====================")
print("================================================")


# for file in FolderListener().list_all_templates():
#     template = ExtractFileText(file).extract_file_text()
#     file_name = file.name.replace("templates/files/", "").replace(".pdf", "")
#     f = open(f"templates/{file_name}.html", "w")
#     f.write(template)
#     f.close()
