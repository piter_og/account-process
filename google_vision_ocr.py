from google_connection import GoogleConnection
from google.cloud import vision
from env import Env
import os


class GoogleVisionOCR(GoogleConnection):
    def __init__(self):
        os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = Env.getenv('VISION_API_FILE')
        self.client = vision.ImageAnnotatorClient()

    def request(self, file):
        image = vision.Image(content=file)

        request = self.client.document_text_detection(image=image)

        return request.full_text_annotation.text.split('\n')
