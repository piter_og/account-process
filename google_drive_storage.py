from googleapiclient.http import MediaFileUpload
from google_connection import GoogleConnection


class GoogleDriveStorage(GoogleConnection):
    def __init__(self, api_name, api_version):
        scope = ['https://www.googleapis.com/auth/drive']

        try:
            self.connection = super().__init__(api_name, api_version, scope)
        except Exception as e:
            print('### Unable to connect.')
            raise e

    def upload(self, file_metadata):
        media = MediaFileUpload(file_metadata['file_name'], mimetype=file_metadata['mime_type'])

        return self.connection.files().create(
            body=file_metadata,
            media_body=media,
            fields='id'
        ).execute()

    def find_folder(self, main_folder_id, search):
        page_token = None
        mime_type = 'application/vnd.google-apps.folder'

        while True:
            response = self.connection.files().list(
                                  q=f"mimeType='{mime_type}' and '{main_folder_id}' in parents and trashed=False",
                                  spaces='drive',
                                  fields='nextPageToken, files(id, name)',
                                  pageToken=page_token)\
                            .execute()

            for file in response.get('files', []):
                if file.get('name') == search:
                    return file.get('id')

            page_token = response.get('nextPageToken', None)
            if page_token is None:
                break

        return False

    def create_folder(self, main_folder_id, folder_name):
        metadata = {
            'name': folder_name,
            'mimeType': 'application/vnd.google-apps.folder',
            'parents': [main_folder_id]
        }

        return self.connection.files().create(body=metadata, fields='id').execute()['id']