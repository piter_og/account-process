from .interfaces import AccountInterface
import tabula as tb


class HealthAccount(AccountInterface):
    def __init__(self):
        self.extracted_file = None
        self.price = None

    def process(self):
        file = tb.read_pdf(self.original_file.name, pages="all", stream=True)
        account_price = self.get_price(file[0])

        super().insert_sheet(account_price)
        super().store()

        return True

    def get_price(self, file):
        self.price = file['Vencimento'].loc[2]
        return self.price

    def template(self) -> str:
        return 'SAUDE'

    def receipt_label(self) -> str:
        return 'IPE SAUDE'