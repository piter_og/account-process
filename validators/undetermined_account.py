from .interfaces import AccountInterface


class UndeterminedAccount(AccountInterface):
    def __init__(self):
        self.file = None

    def template(self) -> str:
        return "UNDEFINED"

    def receipt_label(self) -> str:
        return "UNDEFINED"

    def get_price(self, extracted_text):
        pass

    def process(self):
        return False