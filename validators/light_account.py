from .interfaces import AccountInterface
import re


class LightAccount(AccountInterface):
    def __init__(self):
        self.extracted_file = None
        self.price = None

    def get_price(self, extracted_text):
        find_label = "Valor a pagar   R$ "

        for item in extracted_text.splitlines():
            if find_label in item:
                price = item.replace(find_label, "")
                self.price = re.search('[0-9.,]+', price).group()
                return self.price

        # TODO: send alert informing can´t get the account price
        return False

    def template(self) -> str:
        return 'LUZ'

    def receipt_label(self) -> str:
        return 'CEEE - RS'