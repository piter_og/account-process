from abc import ABC, abstractmethod
from similarity import Similarity
from extract_file_text import ExtractFileText
from env import Env
from drive_connection import DriveConnection
from spreadsheet_connection import SpreadsheetConnection

from datetime import datetime
import os
import mimetypes
import re
from notification_service import NotificationService
import locale
import string


class AccountInterface(ABC):
    def __init__(self):
        self.extracted_file = None
        self.original_file = None
        self.price = None

    def validate(self, file):
        self.original_file = file
        self.extracted_file = ExtractFileText(file).extract_file_text()
        return self.get_similarity() > float(Env.getenv('SIMILARITY_PRECISION'))

    def validate_receipt(self, extracted_file):
        self.extracted_file = extracted_file

        for item in ['Favorecido', 'Favorec']:
            if item in extracted_file:
                index = extracted_file.index(item) + 1
                return self.receipt_label() == extracted_file[index]

        return False

    def get_similarity(self):
        with open(f"templates/{self.template()}.txt") as file:
            extracted_template = file.read()

        sim = Similarity(extracted_template, self.extracted_file).get_similarity()
        self.export_debug(sim)

        return sim

    def process(self):
        account_price = self.get_price(self.extracted_file)

        self.insert_sheet(account_price)
        self.store()

        return True

    def process_receipt(self, file):
        self.original_file = file

        price = self.get_receipt_price()
        due_date = self.get_receipt_due_date()

        cell = self.confirm_informations(price, due_date)

        if cell:
            self.update_receipt_payment(cell)
            self.move_account_receipt()

        return True

    def store(self):
        connection = DriveConnection.connect()
        storage_folder_id = Env.getenv('STORAGE_FOLDER_ID')
        account_folder_id = self.get_or_create_folder_id(connection, storage_folder_id)

        month_folder_name = self.get_folder_month_name()
        month_folder_id = self.get_or_create_folder_id(connection, account_folder_id, month_folder_name)

        file_metadata = {
            'name': 'CONTA.pdf',
            'file_name': self.original_file.name,
            'parents': [month_folder_id],
            'mime_type': mimetypes.guess_type(f'{self.original_file.name}')[0]
        }

        connection.upload(file_metadata)

        message = f"Template: {self.template()} | File: {self.original_file.name} | Price: {self.price}"
        NotificationService().send_message(message)

        if Env.getenv_bool('REMOVE_FILE_AFTER_UPLOAD'):
            os.remove(self.original_file.name)

    def move_account_receipt(self):
        connection = DriveConnection.connect()
        storage_folder_id = Env.getenv('STORAGE_FOLDER_ID')
        account_folder_id = self.get_or_create_folder_id(connection, storage_folder_id)

        month_folder_name = self.get_folder_month_name()
        month_folder_id = self.get_or_create_folder_id(connection, account_folder_id, month_folder_name) ## ESSE METODO ESTA COM ERRO

        file_metadata = {
            'name': 'COMPROVANTE.jpeg',
            'file_name': self.original_file.name,
            'parents': [month_folder_id],
            'mime_type': mimetypes.guess_type(f'{self.original_file.name}')[0]
        }

        connection.upload(file_metadata)

        message = f"Template: {self.template()} | File: {self.original_file.name} | Price: {self.price}"
        NotificationService().send_message(message)

        if Env.getenv_bool('REMOVE_FILE_AFTER_UPLOAD'):
            os.remove(self.original_file.name)

    def get_or_create_folder_id(self, connection, mother_folder_id, folder_name=None):
        folder_id = None
        if not folder_name:
            folder_id = connection.find_folder(mother_folder_id, self.template())
            if not folder_id:
                return connection.create_folder(mother_folder_id, self.template())

        if not folder_id:
            print(f"{mother_folder_id}/{folder_name}")
            print(">>>>")
            folder_id = connection.find_folder(mother_folder_id, folder_name)
            if not folder_id:
                return connection.create_folder(mother_folder_id, folder_name)

        return folder_id

    def insert_sheet(self, price):
        connection = SpreadsheetConnection.connect()
        formated_date = f"{self.get_month_name().strip().lower()}/{datetime.now().year}"
        next_row = self.get_last_row(connection) + 1

        inserted_list = []

        inserted_list.append(connection.update([[self.template()]], f"!A{next_row}"))
        inserted_list.append(connection.update([[price]], f"!B{next_row}"))
        inserted_list.append(connection.update([[formated_date]], f"!C{next_row}", valueInputOption="RAW"))

        return inserted_list

    def get_folder_month_name(self):
        current_month = datetime.now().month
        month_name = self.get_month_name(current_month)
        return f"{'{:0>2}'.format(current_month)} - {month_name.strip()}"

    def get_month_name(self, month=datetime.now().month):
        return Env.getenv('MONTH_LIST').split(',')[month - 1]

    def get_last_row(self, connection):
        values = (connection.getRange().get('valueRanges')[0].get('values'))

        if not values:
            return 0

        return len(values[0])

    def confirm_informations(self, price, due_date):
        connection = SpreadsheetConnection.connect()

        value = (connection.getRange(ranges="!A1:D", majorDimension="ROWS").get('valueRanges')[0].get('values'))

        for index_r, row in enumerate(value):
            for index_c, cell in enumerate(row):
                if len(row) == 3:
                    if row[0] == self.template() \
                            and row[1].replace("R$ ", '').strip() == price \
                            and row[2] == due_date:
                        s_row = index_r + 1
                        s_column = string.ascii_uppercase[index_c + 1]

        try:
            return f"{s_column}{s_row}"
        except NameError:
            return False

    def update_receipt_payment(self, cell):
        connection = SpreadsheetConnection.connect()
        formated_date = datetime.today().strftime("%d/%m/%Y")

        connection.update([[formated_date]], cell)

    def get_receipt_price(self):
        r = re.compile(".*R\$")
        price = list(filter(r.match, self.extracted_file))[0]

        return price.replace("R$ ", '')

    def get_receipt_due_date(self):
        locale.setlocale(locale.LC_ALL, '')

        index = self.extracted_file.index('pagamento')
        date = self.extracted_file[index + 1]

        return datetime.strptime(date, "%d %b %Y - %H:%M:%S").strftime("%B/%Y")

    def export_debug(self, sim):
        if Env.getenv_bool('DEBUG_SIMILARITY'):
            print(f"Similarity: {sim} \nTemplate: {self.template()} \nFile: {self.original_file.name}\n\n")

        return None

    @abstractmethod
    def template(self) -> str:
        pass

    @abstractmethod
    def receipt_label(self) -> str:
        pass

    @abstractmethod
    def get_price(self, extracted_file):
        pass
