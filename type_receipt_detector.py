from validators.light_account import LightAccount
from validators.water_account import WaterAccount
from validators.office_account import OfficeAccount
from validators.net_account import NetAccount
from validators.condominium_account import CondominiumAccount
from validators.health_account import HealthAccount
from validators.undetermined_account import UndeterminedAccount
from notification_service import NotificationService
from google_vision_ocr import GoogleVisionOCR
import io


class TypeReceiptDetector:
    def __init__(self, file):
        self.file = file
        self.original_file = None
        self.extracted_file = None
        self.template_accounts = [
            LightAccount(),
            WaterAccount(),
            NetAccount(),
            OfficeAccount(),
            CondominiumAccount(),
            HealthAccount(),
            UndeterminedAccount()
        ]

    def discover(self):
        with io.open(self.file.name, 'rb') as file:
            content = file.read()

        self.extracted_file = GoogleVisionOCR().request(content)

        for account in self.template_accounts:
            if account.validate_receipt(self.extracted_file):
                account.process_receipt(self.file)

        message = f"File {self.file.name} can´t find correctly receipt template."
        NotificationService().send_message(message)