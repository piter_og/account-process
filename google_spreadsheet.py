from google_connection import GoogleConnection
from dotenv import dotenv_values


class GoogleSpreadsheet(GoogleConnection):
    def __init__(self, api_name, api_version):
        scope = ['https://www.googleapis.com/auth/spreadsheets']

        try:
            self.connection = super().__init__(api_name, api_version, scope)
        except Exception as e:
            print('### Unable to connect.')
            raise e

    def append(self, values, cell="!A1", valueInputOption="USER_ENTERED"):
        print(values, cell, valueInputOption)
        result = self.connection.spreadsheets().values().append(
            spreadsheetId=dotenv_values('.env')["SPREADSHEET_ID"],
            range=cell,
            valueInputOption=valueInputOption,
            insertDataOption="INSERT_ROWS",
            body={'values': values}).execute()

        return result.get("updates").get("updatedCells") > 0

    def update(self, values, cell="!A1", valueInputOption="USER_ENTERED"):
        result = self.connection.spreadsheets().values().update(
                spreadsheetId=dotenv_values('.env')["SPREADSHEET_ID"],
                range=cell,
                valueInputOption=valueInputOption,
                body={'values': values}).execute()

        return result.get('updatedCells') > 0

    def getRange(self, ranges="!A1:A", majorDimension="COLUMNS"):
        result = self.connection.spreadsheets().values().batchGet(spreadsheetId=dotenv_values('.env')["SPREADSHEET_ID"],
                                                                  majorDimension=majorDimension,
                                                                   ranges=ranges).execute()

        return result