from dotenv import dotenv_values
from google_drive_storage import GoogleDriveStorage


class DriveConnection:
    @staticmethod
    def connect(
            api_name=dotenv_values('.env')["STORAGE_API_NAME"],
            api_version=dotenv_values('.env')["STORAGE_API_VERSION"]):
        return GoogleDriveStorage(api_name, api_version)
