from pdfminer.high_level import extract_text
from wand.image import Image as wi
from PIL import Image
import pytesseract
import io


class ExtractFileText:

    def __init__(self, file):
        self.file = file

    def extract_file_text(self):
        if isinstance(self.file, io.TextIOWrapper):
            file_name = self.file.name
        else:
            file_name = self.file

        text = extract_text(file_name)

        if len(text) == 1:
            text = self.convert_image_pdf_to_text(file_name)

        return text

    def convert_image_pdf_to_text(self, file_name):
        pdf_file = wi(filename=file_name, resolution=800)
        image = pdf_file.convert('jpeg')

        image_blobs = []
        for img in image.sequence:
            img_page = wi(image=img)
            image_blobs.append(img_page.make_blob('jpeg'))

        for img_blob in image_blobs:
            image = Image.open(io.BytesIO(img_blob))
            text = pytesseract.image_to_string(image)

        return text
