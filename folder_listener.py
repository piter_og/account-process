from dotenv import dotenv_values
import os


class FolderListener:
    def __init__(self):
        self.__template_folder = dotenv_values('.env')['LOCAL_FOLDER_TEMPLATE']
        self.__storage_folder = dotenv_values('.env')['LOCAL_FOLDER_STORAGE']
        self.__storage_receipt = dotenv_values('.env')['LOCAL_FOLDER_RECEIPT']

    def list_all_files(self):
        return self.__list_files(self.__storage_folder)

    def list_all_templates(self):
        return self.__list_files(self.__template_folder)

    def list_all_receipt(self):
        return self.__list_files(self.__storage_receipt)

    def __list_files(self, folder):
        data = []

        for root, dirs, files in os.walk(folder):
            for file in files:
                with open(os.path.join(root, file), "r") as auto:
                    data.append(auto)

        return data
