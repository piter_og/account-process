from dotenv import dotenv_values


class Env:
    @staticmethod
    def getenv(name: str, default: str = "") -> str:
        try:
            return dotenv_values('.env')[name]
        except KeyError:
            return default

    @staticmethod
    def getenv_bool(name: str, default: bool = False) -> bool:
        try:
            var = dotenv_values('.env')[name]
        except KeyError:
            return default
        try:
            return bool(int(var))
        except ValueError:
            if str(var).lower() in ('true', 'yes', 'y', 'on'):
                return True
            elif str(var).lower() in ('false', 'no', 'n', 'off'):
                return False
            else:
                return default