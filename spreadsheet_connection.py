from dotenv import dotenv_values
from google_spreadsheet import GoogleSpreadsheet


class SpreadsheetConnection:
    @staticmethod
    def connect(api_name=dotenv_values('.env')["SPREADSHEET_API_NAME"], api_version=dotenv_values('.env')["SPREADSHEET_API_VERSION"]):
        return GoogleSpreadsheet(api_name, api_version)
