import spacy


class Similarity:
    def __init__(self, template, file):
        self.template = template
        self.file = file

    def get_similarity(self):
        nlp = spacy.load('pt_core_news_lg')

        template = nlp(f'{self.template}')
        file = nlp(f'{self.file}')

        return template.similarity(file)
