from validators.light_account import LightAccount
from validators.water_account import WaterAccount
from validators.office_account import OfficeAccount
from validators.net_account import NetAccount
from validators.condominium_account import CondominiumAccount
from validators.health_account import HealthAccount
from validators.undetermined_account import UndeterminedAccount
from notification_service import NotificationService


class TypeDetector:
    def __init__(self, file):
        self.file = file
        self.template_accounts = [
            LightAccount(),
            WaterAccount(),
            NetAccount(),
            OfficeAccount(),
            CondominiumAccount(),
            HealthAccount(),
            UndeterminedAccount()
        ]

    def discover(self):
        for account in self.template_accounts:
            if account.validate(self.file):
                return account.process()

        message = f"File {self.file.name} can´t find correctly template."
        NotificationService().send_message(message)