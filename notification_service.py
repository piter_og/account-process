from env import Env
import requests


class NotificationService:

    def mount(self, method, message) -> str:
        token = Env().getenv('TELEGRAM_BOT_TOKEN')
        endpoint = f"{Env().getenv('TELEGRAM_ENPOINT')}{token}{method}"
        chat_id = Env().getenv('TELEGRAM_CHAT_ID')
        text = ""
        if not message == "":
            text = f"&text={message}"

        return f"{endpoint}?chat_id={chat_id}{text}"

    def send_message(self, message):
        method = '/sendMessage'
        text = message

        return requests.get(self.mount(method, text))
